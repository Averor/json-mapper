<?php

namespace Averor\JsonMapper;

use Assert\Assert;
use ReflectionClass;

/**
 * Class JsonMapper
 *
 * @package Averor\JsonMapper
 * @author Averor <averor.dev@gmail.com>
 */
class JsonMapper
{
    /**
     * @param string $json
     * @param string $fqcn
     * @return object|array
     * @throws \ReflectionException
     */
    public static function map(string $json, string $fqcn)
    {
        Assert::that($json)->isJsonString("No valid json given");
        Assert::that($fqcn)->classExists("Class %s does not exist");

        $reflectionClass = new ReflectionClass($fqcn);

        $decoded = json_decode($json, true);

        Assert::that($decoded)->isArray("json does not represent an array");

        return static::mapObject($decoded, $reflectionClass);
    }

    /**
     * @param string $json
     * @param string $fqcn
     * @return array
     * @throws \ReflectionException
     */
    public static function mapArray(string $json, string $fqcn) : array
    {
        Assert::that($json)->isJsonString("No valid json given");
        Assert::that($fqcn)->classExists("Class %s does not exist");

        $reflectionClass = new ReflectionClass($fqcn);

        $decoded = json_decode($json, true);

        Assert::that($decoded)->isArray("json does not represent an array");

        $return = [];

        foreach ($decoded as $array) {
            $return[] = static::mapObject($array, $reflectionClass);;
        }

        return $return;
    }

    /**
     * @param array $decoded
     * @param ReflectionClass $reflectionClass
     * @return object
     */
    protected static function mapObject(array $decoded, ReflectionClass $reflectionClass)
    {
        $object = $reflectionClass->newInstanceWithoutConstructor();

        foreach ($decoded as $property => $value) {

            Assert::that($property)->string("Json array key must be string");

            if (!$reflectionClass->hasProperty($property)) {
                continue;
            }

            $property = $reflectionClass->getProperty($property);
            $property->setAccessible(true);
            $property->setValue($object, $value);
        }

        return $object;
    }
}
