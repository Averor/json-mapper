# Averor/json-mapper

[![pipeline status](https://gitlab.com/Averor/json-mapper/badges/master/pipeline.svg)](https://gitlab.com/Averor/json-mapper/commits/master)
[![coverage report](https://gitlab.com/Averor/json-mapper/badges/master/coverage.svg)](https://gitlab.com/Averor/json-mapper/commits/master)
[![MIT License](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://gitlab.com/Averor/json-mapper/blob/master/LICENSE)
[![Semver](http://img.shields.io/SemVer/1.0.0.png)](http://semver.org/spec/v1.0.0.html)

Mapping from json to given class or array of classes

## Install using Composer:

`composer require averor/json-mapper`

## Usage:

###### *Single object*
```php
object \Averor\JsonMapper\JsonMapper::map(string $json, string $fqcn);
```

###### *Array of objects*
```php
array \Averor\JsonMapper\JsonMapper::mapArray(string $json, string $fqcn);
```

## Testing with PHPUnit (>=6.0)
`$ ./vendor/bin/phpunit ./tests` 