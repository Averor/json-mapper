<?php

namespace Averor\JsonMapper\Tests;

use Averor\JsonMapper\JsonMapper;
use Averor\JsonMapper\Tests\Resources\TestMappedClass;
use Averor\JsonMapper\Tests\Resources\TestMappedClass1;
use Averor\JsonMapper\Tests\Resources\TestMappedClass2;
use PHPUnit\Framework\TestCase;

/**
 * Class JsonMapperTest
 *
 * @package Averor\JsonMapper\Tests
 * @author Averor <averor.dev@gmail.com>
 */
class JsonMapperTest extends TestCase
{
    public function validMapValuesProvider() : array
    {
        return [

            'one object with 1:1 properties' => [
                '{"foo":"str","bar":1,"baz":true}',
                TestMappedClass1::class,
                new TestMappedClass1("str", 1, true),
            ],
            'one object with 1:1 properties - with inheritance' => [
                '{"foo":"str","bar":1,"baz":true}',
                TestMappedClass2::class,
                new TestMappedClass2("str", 1, true),
            ],


            'one object with more properties' => [
                '{"foo":"str","bar":1,"other":"other string","baz":true}',
                TestMappedClass1::class,
                new TestMappedClass1("str", 1, true),
            ],
            'one object with more properties - with inheritance' => [
                '{"foo":"str","bar":1,"other":"other string","baz":true}',
                TestMappedClass2::class,
                new TestMappedClass2("str", 1, true),
            ],

            'one object without all properties' => [
                '{"foo":"str","other":1,"baz":true}',
                TestMappedClass1::class,
                new TestMappedClass1("str", 0, true),
            ],
            'one object without all properties - with inheritance' => [
                '{"foo":"str","other":1,"baz":true}',
                TestMappedClass2::class,
                new TestMappedClass2("str", 0, true),
            ],

        ];
    }

    public function validMapArrayValuesProvider() : array
    {
        return [

            'array of objects with 1:1 properties' => [
                '[{"foo":"str","bar":1,"baz":true},{"foo":"str2","bar":2,"baz":true}]',
                TestMappedClass1::class,
                [
                    new TestMappedClass1("str", 1, true),
                    new TestMappedClass1("str2", 2, true)
                ],
            ],
            'array of objects with 1:1 properties - with inheritance' => [
                '[{"foo":"str","bar":1,"baz":true},{"foo":"str2","bar":2,"baz":true}]',
                TestMappedClass2::class,
                [
                    new TestMappedClass2("str", 1, true),
                    new TestMappedClass2("str2", 2, true)
                ],
            ],


            'array of objects with more properties' => [
                '[{"foo":"str","bar":1,"other":"other string","baz":true},{"other":"other string 2","foo":"str2","bar":2,"baz":true}]',
                TestMappedClass1::class,
                [
                    new TestMappedClass1("str", 1, true),
                    new TestMappedClass1("str2", 2, true),
                ],
            ],
            'array of objects with more properties - with inheritance' => [
                '[{"foo":"str","bar":1,"other":"other string","baz":true},{"other":"other string 2","foo":"str2","bar":2,"baz":true}]',
                TestMappedClass2::class,
                [
                    new TestMappedClass2("str", 1, true),
                    new TestMappedClass2("str2", 2, true),
                ],
            ],

            'array of objects without all properties' => [
                '[{"foo":"str","baz":true},{"foo":"str2","bar":2}]',
                TestMappedClass1::class,
                [
                    new TestMappedClass1("str", 0, true),
                    new TestMappedClass1("str2", 2, false)
                ],
            ],
            'array of objects without all properties - with inheritance' => [
                '[{"foo":"str","baz":true},{"foo":"str2","bar":2}]',
                TestMappedClass2::class,
                [
                    new TestMappedClass2("str", 0, true),
                    new TestMappedClass2("str2", 2, false)
                ],
            ],

            'array of objects - mixed' => [
                '[{"foo":"str","bar":1,"baz":true},{"foo":"str2","bar":2,"other":"other string","baz":false},{"foo":"str3","baz":true}]',
                TestMappedClass1::class,
                [
                    new TestMappedClass1("str", 1, true),
                    new TestMappedClass1("str2", 2, false),
                    new TestMappedClass1("str3", 0, true),
                ],
            ],
            'array of objects - mixed - with inheritance' => [
                '[{"foo":"str","bar":1,"baz":true},{"foo":"str","bar":1,"other":"other string","baz":true},{"foo":"str","baz":true}]',
                TestMappedClass2::class,
                [
                    new TestMappedClass2("str", 1, true),
                    new TestMappedClass2("str", 1, true),
                    new TestMappedClass2("str", 0, true),
                ],
            ],

        ];
    }

    public function invalidValuesProvider() : array
    {
        return [
            'invalid json #1' => ['{"foo"}', TestMappedClass2::class],
            'invalid json #2' => ['', TestMappedClass2::class],
            'invalid json #3' => ['str', TestMappedClass2::class],
            'invalid json #4' => [1, TestMappedClass2::class],
            'non-existent class' => ['{"foo":"bar"}', 'Non\Existent\Class']
        ];
    }

    /**
     * @dataProvider validMapValuesProvider
     * @param string $json
     * @param string $fqcn
     * @param TestMappedClass $expected
     * @throws \ReflectionException
     */
    public function testMap(string $json, string $fqcn, TestMappedClass $expected)
    {
        /** @var TestMappedClass $actual */
        $actual = JsonMapper::map($json, $fqcn);

        $this->assertEquals(
            $expected,
            $actual
        );
    }

    /**
     * @dataProvider validMapArrayValuesProvider
     * @param string $json
     * @param string $fqcn
     * @param TestMappedClass[] $expected
     * @throws \ReflectionException
     */
    public function testMapArray(string $json, string $fqcn, array $expected)
    {
        /** @var TestMappedClass $actual */
        $actual = JsonMapper::mapArray($json, $fqcn);

        foreach ($actual as $idx => $object) {
            $this->assertEquals(
                $expected[$idx],
                $object
            );
        }
    }

    /**
     * @dataProvider invalidValuesProvider
     * @expectedException \Assert\AssertionFailedException
     * @param $json
     * @param $fqcn
     * @throws \ReflectionException
     */
    public function testMapExceptions($json, $fqcn)
    {
        /** @var TestMappedClass $mappedObject */
        JsonMapper::map($json, $fqcn);
    }

    /**
     * @dataProvider invalidValuesProvider
     * @expectedException \Assert\AssertionFailedException
     * @param $json
     * @param $fqcn
     * @throws \ReflectionException
     */
    public function testMapArrayExceptions($json, $fqcn)
    {
        /** @var TestMappedClass $mappedObject */
        JsonMapper::mapArray($json, $fqcn);
    }
}
