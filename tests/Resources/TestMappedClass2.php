<?php

namespace Averor\JsonMapper\Tests\Resources;

/**
 * Class TestMappedClass2
 *
 * @package Averor\JsonMapper\Tests\Resources
 * @author Averor <averor.dev@gmail.com>
 */
class TestMappedClass2 extends TestAbstractClass2 implements TestMappedClass
{
    /** @var bool */
    private $baz = false;

    public function __construct(string $foo, int $bar, bool $baz)
    {
        parent::__construct($foo, $bar);

        $this->baz = $baz;
    }

    public function getBaz() : bool
    {
        return $this->baz;
    }
}
