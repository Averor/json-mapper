<?php

namespace Averor\JsonMapper\Tests\Resources;

/**
 * Interface TestMappedClass
 *
 * @package Averor\JsonMapper\Tests\Resources
 * @author Averor <averor.dev@gmail.com>
 */
interface TestMappedClass
{
    public function __construct(string $foo, int $bar, bool $baz);
    public function getFoo() : string;
    public function getBar() : int;
    public function getBaz() : bool;
}
