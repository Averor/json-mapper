<?php

namespace Averor\JsonMapper\Tests\Resources;

/**
 * Class TestAbstractClass2
 *
 * @package Averor\JsonMapper\Tests\Resources
 * @author Averor <averor.dev@gmail.com>
 */
abstract class TestAbstractClass2
{
    /** @var string */
    public $foo = 'none';

    /** @var int */
    protected $bar = 0;

    public function __construct(string $foo, int $bar)
    {
        $this->foo = $foo;
        $this->bar = $bar;
    }

    public function getFoo() : string
    {
        return $this->foo;
    }

    public function getBar() : int
    {
        return $this->bar;
    }
}
