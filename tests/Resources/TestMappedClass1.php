<?php

namespace Averor\JsonMapper\Tests\Resources;

/**
 * Class TestMappedClass1
 *
 * @package Averor\JsonMapper\Tests\Resources
 * @author Averor <averor.dev@gmail.com>
 */
class TestMappedClass1 implements TestMappedClass
{
    /** @var string */
    public $foo = 'none';

    /** @var int */
    public $bar = 0;

    /** @var bool */
    private $baz = false;

    public function __construct(string $foo, int $bar, bool $baz)
    {
        $this->foo = $foo;
        $this->bar = $bar;
        $this->baz = $baz;
    }

    public function getFoo() : string
    {
        return $this->foo;
    }

    public function getBar() : int
    {
        return $this->bar;
    }

    public function getBaz() : bool
    {
        return $this->baz;
    }
}
